cmake_minimum_required(VERSION 3.5)

project (StepperTesting)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -lwiringPi")
set(SOURCES main.cc)

add_executable(main ${SOURCES})

